# Written by: Kevin Baker
# June 15, 2018
from pyfader import GSFader
import pygame, sys, socket, threading
from multiprocessing import Queue
from pygame.locals import *


msgQueue = Queue()

######################################## FUNCTIONS THAT KEVIN DIDN'T WRITE ###########################################
class TextRectException:
    def __init__(self, message = None):
        self.message = message
    def __str__(self):
        return self.message

# God fuckin' damnit this function saved my ass!
def render_textrect(string, font, rect, text_color, background_color, justification=0):
    """Returns a surface containing the passed text string, reformatted
    to fit within the given rect, word-wrapping as necessary. The text
    will be anti-aliased.

    Takes the following arguments:

    string - the text you wish to render. \n begins a new line.
    font - a Font object
    rect - a rectstyle giving the size of the surface requested.
    text_color - a three-byte tuple of the rgb value of the
                 text color. ex (0, 0, 0) = BLACK
    background_color - a three-byte tuple of the rgb value of the surface.
    justification - 0 (default) left-justified
                    1 horizontally centered
                    2 right-justified

    Returns the following values:

    Success - a surface object with the text rendered onto it.
    Failure - raises a TextRectException if the text won't fit onto the surface.
    """

    import pygame
    
    final_lines = []

    requested_lines = string.splitlines()

    # Create a series of lines that will fit on the provided
    # rectangle.

    for requested_line in requested_lines:
        if font.size(requested_line)[0] > rect.width:
            words = requested_line.split(' ')
            # if any of our words are too long to fit, return.
            for word in words:
                if font.size(word)[0] >= rect.width:
                    raise TextRectException("The word " + word + " is too long to fit in the rect passed.")
            # Start a new line
            accumulated_line = ""
            for word in words:
                test_line = accumulated_line + word + " "
                # Build the line while the words fit.    
                if font.size(test_line)[0] < rect.width:
                    accumulated_line = test_line 
                else: 
                    final_lines.append(accumulated_line) 
                    accumulated_line = word + " " 
            final_lines.append(accumulated_line)
        else: 
            final_lines.append(requested_line) 

    # Let's try to write the text out on the surface.

    surface = pygame.Surface(rect.size) 
    surface.fill(background_color) 

    accumulated_height = 0 
    for line in final_lines: 
        if accumulated_height + font.size(line)[1] >= rect.height:
            raise TextRectException("Once word-wrapped, the text string was too tall to fit in the rect.")
        if line != "":
            tempsurface = font.render(line, 1, text_color)
            if justification == 0:
                surface.blit(tempsurface, (0, accumulated_height))
            elif justification == 1:
                surface.blit(tempsurface, ((rect.width - tempsurface.get_width()) / 2, accumulated_height))
            elif justification == 2:
                surface.blit(tempsurface, (rect.width - tempsurface.get_width(), accumulated_height))
            else:
                raise TextRectException("Invalid justification argument: " + str(justification))
        accumulated_height += font.size(line)[1]

    return surface
###############################################################################################################






################################# FUCNTIONS THAT KEVIN WROTE ##################################################
def getConnection():
    # Create an AF_INET (IPv4), STREAM (TCP) socket
    connectedsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # server networking information
    host, port = "localhost", 12345

    try:
        # Bind the socket to the network address
        connectedsocket.bind((host, port))
    except socket.error as msg:
        print( 'Binding failed: {}'.format(msg) )
        sys.exit()

    # Queue up to 2 requests
    print("Waiting for connection...")
    connectedsocket.listen(1)
    connection, address = connectedsocket.accept()
    print( "Connection from " + str(address) )
    print( "Receiving data...")
    return connection

def messangerBoy(terminalConnection):
    while True:
        message = terminalConnection.recv(1024).decode('utf-8')
        if message == "quit\n":
            msgQueue.put(message)
            sys.exit()
        msgQueue.put(message)
###############################################################################################################




######################################## MAIN PROGRAM #########################################################
# Local Variables
rez = (1920,1080)
rectangle= (1820,980)
font_size = 120
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
offset_text_from_top_left = (50,50)

# Create worker thread for handling TCP/IP messages
terminal = getConnection()
thread = threading.Thread(target = messangerBoy, args = (terminal,) )
thread.start()


# Create window for display text
pygame.init()
window = pygame.display.set_mode((0,0), pygame.FULLSCREEN) # This is my window, setting it to screen size
width, height = pygame.display.Info().current_w, pygame.display.Info().current_h
window = pygame.display.set_mode( (width,height), pygame.NOFRAME )
#window = pygame.display.set_mode(rez)
pygame.display.set_caption("Kevin's Gift for D&D") # Set window title
window.fill( BLACK ) # Fill screen with color black
#message = "Welcome to the game."
first_msg = True
old_message = None
f = None


while True:

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                pygame.quit()
                sys.exit()

    # If there is a message to be displayed
    if msgQueue.empty() == False:

        if first_msg == True:
            message = msgQueue.get()
            first_msg = False
        else:
            old_message = message
            message = msgQueue.get()

        if message == "quit\n":
            pygame.quit()
            sys.exit()

        # Create text surface with old text
        if old_message != None:
            # Fade out old text
            for repeat in range(200):
                f.fadeOut(2)
                f.draw( window,offset_text_from_top_left )
                pygame.display.update()

        # Wait a second to fade in new text
        #pygame.time.delay(500)

        # Create text surface with new text
        my_font = pygame.font.Font(None, font_size)
        my_rect = pygame.Rect( offset_text_from_top_left + (rectangle) )
        rendered_text = render_textrect(message, my_font, my_rect, GREEN, BLACK, 0)
        rendered_text.set_alpha(0) # text will be transparent to begin with
        if rendered_text:
            # Fade in new text
            f = GSFader( rendered_text, BLACK, 0)
            for repeat in range(200):
                f.fadeIn(2)
                f.draw( window, offset_text_from_top_left )
                pygame.display.update()
    else:
        None
###############################################################################################################
