# Written by: Kevin Baker
# June 15, 2018
import socket
import sys
import time
import os
import select


######################################## MAIN PROGRAM #########################################################
print ("With great power comes great responsibility...")

# Create TCP connection between client and server
try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error as msg:
    print('Socket creation Failed. Error code:{}'.format(str(msg)) )
    sys.exit()


HOST = 'localhost' # remote host
PORT = 12345        # port number that display is listening on
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:

    print("Connecting to Display...")
    s.connect( (HOST, PORT) ) # Blocks until it makes a connection to display
    print ("Connected.")
    print ("You may now send data!")
    #s.send( "{}".format(name).encode() )

    running = True
    while running:

        for line in sys.stdin:
            if line == "quit\n":
                data = line
                s.send("{}".format(data).encode())
                s.close()
                sys.exit()
            else:
                data = line
                s.send("{}".format(data).encode())

    s.close()
###############################################################################################################
