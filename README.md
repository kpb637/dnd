# DnD Display Program

This is a program that displays text input from a terminal in a different specialized window


Instructions to run:
1) Install python
2) run command below to install pygame modules
   python -m pip install pygame
3) input command into powershell -> python .\display.py
4) input command ito different powershell -> python .\terminal.py

Instructions while program is running:
1) Type quit into terminal.py to quit both programs at once
2) send carage return (aka press enter) to make current text fade out to black
   OR
   Send a new message to make current text fade out to black and have new text fade in


# Reference for Kevin
git commit first to make changes
git push after to push to repo